import React from "react";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import { Navigate } from "react-router-dom";
import addIcon from "../icons/add-icon.png";
import favoriteIcon from "../icons/favorites-icon.png";
function Recipes() {
  const user = useSelector((state) => state.auth.user);

  const [recipes, setRecipes] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [recipeTypes, setRecipeTypes] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [addedToShoppingList, setAddedToShoppingList] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8080/recipes").then((data) => {
      setRecipes(data.data);
      const types = [...new Set(data.data.map((recipe) => recipe.type))];
      setRecipeTypes(types);
    });
    if (user) {
      axios.get(`http://localhost:8080/favorites/${user}`).then((data) => {
        setFavorites(data.data.map((fav) => fav.recipeId));
      });
    }
  }, [user]);

  useEffect(() => {
    axios.get("http://localhost:8080/recipes/ingredients/").then((data) => {
      setIngredients(data.data);
    });
  }, []);

  let ingredientArr = [];

  const getRecipesByType = (type) => {
    type === "all"
      ? axios.get("http://localhost:8080/recipes").then((data) => {
          setRecipes(data.data);
        })
      : axios.get(`http://localhost:8080/recipes/${type}`).then((data) => {
          setRecipes(data.data);
        });
  };

  const addFavorite = (recipeId) => {
    axios
      .post("http://localhost:8080/favorites", {
        username: user,
        recipeId: recipeId,
      })
      .then(() => {
        setFavorites([...favorites, recipeId]);
      })
      .catch((error) => console.error("Error adding favorite:", error));
  };

  const submitHandler = (event, recipeId) => {
    event.preventDefault();
    const selectedRecipeId = recipeId;
    ingredientArr.length = 0;

    const selectedIngredients = ingredients.filter(
      (ingredient) => ingredient.recipeId === parseInt(selectedRecipeId)
    );
    selectedIngredients.map((ingredient) => {
      ingredientArr.push({
        username: user,
        ingredientId: ingredient.ingredientId,
        quantity: ingredient.quantity,
        unit: ingredient.unit,
      });
    });

    ingredientArr.forEach((ingredient) => {
      axios.post("http://localhost:8080/shoppinglist", ingredient);
      //Need error handling please
    });
    setAddedToShoppingList([...addedToShoppingList, selectedRecipeId]);
  };
  return (

    <div style={{ paddingBottom: '65px' }} className="container mx-auto mt-8">
      <label htmlFor="recipeType" className="text-lg font-semibold block mb-2">
        Filter by type
      </label>
      <select
        id="recipeType"
        onChange={(e) => getRecipesByType(e.target.value)}
        className="bg-white border border-gray-400 rounded-lg px-4 py-2 w-full"
      >
        <option value="all">All</option>
        {recipeTypes.map((type, index) => (
          <option key={index} value={type}>
            {type}
          </option>
        ))}
      </select>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 mt-4">
        {recipes.map((recipe, index) => {
          const isFavorited = favorites.includes(recipe.recipeId);
          const isAddedToShoppingList = addedToShoppingList.includes(
            recipe.recipeId
          );
          return (
            <div
              key={index}
              className="max-w-lg mx-auto bg-white shadow-md rounded-lg overflow-hidden mb-4 relative"
            >
              <img
                className="w-full h-64 object-cover"
                src={require(`../images/recipes/${recipe.imageName}`)}
                alt={recipe.imageAlt}
              />
              <div className="p-4">
                <h2 className="text-2xl font-semibold mb-2">{recipe.title}</h2>
                <h3 className="text-lg font-medium mb-2">Ingredients:</h3>
                <ul className="list-disc pl-5 mb-4">
                  {ingredients.map((ingredient, index) =>
                    recipe.recipeId === ingredient.recipeId ? (
                      <li key={index}>{`${ingredient.quantity} ${
                        ingredient.quantity > 1
                          ? ingredient.name + "s"
                          : ingredient.name
                      } ${ingredient.unit ? ingredient.unit : ""}`}</li>
                    ) : null
                  )}
                </ul>
                <h3 className="text-lg font-medium mb-2">Instructions:</h3>
                <p className="mb-4">{recipe.cookingSteps}</p>
                <form
                  id={recipe.recipeId}
                  onSubmit={(e) => submitHandler(e, recipe.recipeId)}
                >
                  <button
                    type="submit"
                    className="bg-green-500 text-white py-2 px-4 rounded-lg flex items-center"
                    isAddedToShoppingList
                  >
                    {isAddedToShoppingList ? (
                      "Add again"
                    ) : (
                      <>
                        <img
                          src={addIcon}
                          alt="Add to Shopping List button"
                          className="h-8 w-8 mr-2"
                        />
                        <span>Shopping List</span>
                      </>
                    )}
                  </button>
                </form>

                <button
                  onClick={() => addFavorite(recipe.recipeId)}
                  className={`absolute bottom-4 right-4 py-2 px-4 rounded-lg flex items-center ${
                    isFavorited
                      ? "bg-gray-500 text-white"
                      : "bg-red-500 text-white"
                  }`}
                >
                  {isFavorited ? (
                    "Favorited"
                  ) : (
                    <>
                      <img
                        alt="Add to Favorites button"
                        className="h-8 w-8 mr-2"
                        src={favoriteIcon}
                      ></img>
                      <span>Add to Favorites</span>
                    </>
                  )}
                </button>
              </div>
            </div>
          );
        })}
      </div>
      {!user && <Navigate to="/" replace={true} />}
    </div>
  );
}

export default Recipes;
