import React from "react";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import { Navigate } from "react-router-dom";
import addIcon from "../icons/add-icon.png";
import deleteIcon from "../icons/delete-icon.png";

function Favorites() {
  const username = useSelector((state) => state.auth.user);

  const [recipes, setRecipes] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [recipeTypes, setRecipeTypes] = useState([]);

  //get all favorite recipes
  useEffect(() => {
    axios.get(`http://localhost:8080/favorites/${username}`).then((data) => {
      setRecipes(data.data);
      const types = [...new Set(data.data.map((recipe) => recipe.type))];
      setRecipeTypes(types);
    });
  }, []);

  useEffect(() => {
    axios.get("http://localhost:8080/recipes/ingredients/").then((data) => {
      setIngredients(data.data);
    });
  }, []);

  let ingredientArr = [];

  const getRecipesByType = (type) => {
    type == "all"
      ? axios
          .get(`http://localhost:8080/favorites/${username}`)
          .then((data) => {
            setRecipes(data.data);
          })
      : axios
          .get(`http://localhost:8080/favorites/${username}/${type}`)
          .then((data) => {
            setRecipes(data.data);
          });
  };

  const removeFavorite = (selectedRecipeId) => {
    axios
      .delete(`http://localhost:8080/favorites/${selectedRecipeId}`)
      .then(() =>
        axios
          .get(`http://localhost:8080/favorites/${username}`)
          .then((data) => {
            setRecipes(data.data);
            const types = [...new Set(data.data.map((recipe) => recipe.type))];
            setRecipeTypes(types);
          })
      );
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const selectedRecipeId = event.target.id;
    ingredientArr.length = 0;

    const selectedIngredients = ingredients.filter(
      (ingredient) => ingredient.recipeId === parseInt(selectedRecipeId)
    );
    selectedIngredients.map((ingredient) => {
      ingredientArr.push({
        username: username,
        ingredientId: ingredient.ingredientId,
        quantity: ingredient.quantity,
        unit: ingredient.unit,
      });
    });

    ingredientArr.forEach((ingredient) => {
      axios.post("http://localhost:8080/shoppinglist", ingredient);
      //Need error handling
    });
    alert("Ingredients added to shopping list");
  };

  return (
    <div style={{ paddingBottom: "65px" }} className="container mx-auto mt-8">
      <label htmlFor="recipeType" className="text-lg font-semibold block mb-2">
        Filter by type
      </label>
      <select
        id="recipeType"
        onChange={(e) => getRecipesByType(e.target.value)}
        className="bg-white border border-gray-400 rounded-lg px-4 py-2 w-full"
      >
        <option value="all">All</option>
        {recipeTypes.map((type, index) => (
          <option key={index} value={type}>
            {type}
          </option>
        ))}
      </select>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 mt-4">
        {recipes.map((recipe, index) => {
          return (
            <div
              key={index}
              className="max-w-lg mx-auto bg-white shadow-md rounded-lg overflow-hidden mb-4 relative"
            >
              <img
                className="w-full h-64 object-cover"
                src={require(`../images/recipes/${recipe.imageName}`)}
                alt={recipe.imageAlt}
              />
              <div className="p-4">
                <h2 className="text-2xl font-semibold mb-2">{recipe.title}</h2>
                <h3 className="text-lg font-medium mb-2">Ingredients:</h3>
                <ul className="list-disc pl-5 mb-4">
                  {ingredients.map((ingredient, index) =>
                    recipe.recipeId === ingredient.recipeId ? (
                      <li key={index}>{`${ingredient.quantity} ${
                        ingredient.quantity > 1
                          ? ingredient.name + "s"
                          : ingredient.name
                      } ${ingredient.unit ? ingredient.unit : ""}`}</li>
                    ) : null
                  )}
                </ul>
                <h3 className="text-lg font-medium mb-2">Instructions:</h3>
                <p className="mb-4">{recipe.cookingSteps}</p>
                <form id={recipe.recipeId} onSubmit={submitHandler}>
                  <button
                    type="submit"
                    className="bg-green-500 text-white py-2 px-4 rounded-lg flex items-center hover:bg-green-400"
                  >
                    <img
                      src={addIcon}
                      alt="Add to Shopping List button"
                      className="h-8 w-8 mr-2"
                    />
                    <span>Shopping List</span>
                  </button>
                </form>
              </div>
              <button
                onClick={() => removeFavorite(recipe.favoriteId)}
                className="bg-red-500 text-white py-2 px-4 rounded-lg flex items-center hover:bg-red-400 absolute bottom-4 right-4"
              >
                <img
                  src={deleteIcon}
                  alt="Delete from Favorites button"
                  className="h-8 w-8 mr-2"
                />
                <span>Remove</span>
              </button>
            </div>
          );
        })}
      </div>
      {!username && <Navigate to="/" replace={true} />}
    </div>
  );
}

export default Favorites;
