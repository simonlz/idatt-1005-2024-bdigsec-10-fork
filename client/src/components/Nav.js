import React, { useState, useRef } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import dropdownImage from "../icons/dropdown.png";
import homeIcon from "../icons/home-icon.png";
import profileIcon from "../icons/profile-icon.png";

export default function Navbar() {
  const loggedIn = useSelector((state) => state.auth.isLoggedIn);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const dropdownTimeoutRef = useRef(null);

  const handleMouseEnter = () => {
    setDropdownOpen(true);
    clearTimeout(dropdownTimeoutRef.current);
  };

  const handleMouseLeave = () => {
    dropdownTimeoutRef.current = setTimeout(() => {
      setDropdownOpen(false);
    }, 150);
  };

  const handleDropdownMouseEnter = () => {
    clearTimeout(dropdownTimeoutRef.current);
  };

  const handleDropdownMouseLeave = () => {
    setDropdownOpen(false);
  };

  const DropdownContent = () => (
    <div
      onMouseEnter={handleDropdownMouseEnter}
      onMouseLeave={handleDropdownMouseLeave}
      className="absolute top-10 right-5 mt-2 bg-gray-800 text-white rounded-md shadow-lg"
    >
      <ul className="py-1">
        <li>
          <Link to="/recipes" className="block px-4 py-2 hover:bg-gray-700">
            Recipes
          </Link>
        </li>
        {loggedIn && (
          <>
            <li>
              <Link
                to="/shoppinglist"
                className="block px-4 py-2 hover:bg-gray-700"
              >
                Shopping List
              </Link>
            </li>
            <li>
              <Link
                to="/inventory"
                className="block px-4 py-2 hover:bg-gray-700"
              >
                Inventory
              </Link>
            </li>
            <li>
              <Link
                to="/favorites"
                className="block px-4 py-2 hover:bg-gray-700"
              >
                Favorites
              </Link>
            </li>
          </>
        )}
      </ul>
    </div>
  );

  return (
    <nav className="flex items-center justify-between flex-wrap bg-green-800 p-6">
      <div className="flex items-center flex-shrink-0 text-white mr-6 relative">
        <button
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          className="hidden lg:flex items-center flex-shrink-0 text-white mr-4"
        >
          <img src={dropdownImage} alt="dropdown" className="w-8 h-8" />
        </button>
        {dropdownOpen && <DropdownContent />}
        <Link
          to="/"
          className="flex items-center flex-shrink-0 text-white pl-2 mr-4"
        >
          <img src={homeIcon} alt="home" className="w-8 h-8" />
        </Link>
      </div>

      <div>
        <Link to="/profile" className="text-white">
          <img src={profileIcon} alt="profile" className="w-8 h-8" />
        </Link>
      </div>
    </nav>
  );
}
