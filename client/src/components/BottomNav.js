import React from "react";
import { Link, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import recipesIcon from '../icons/recipes-icon.png';
import inventoryIcon from '../icons/inventory-icon.png';
import favoritesIcon from '../icons/favorites-icon.png';
import shoppinglistIcon from '../icons/shoppinglist-icon.png';

export default function Bottomnavbar() {
  const loggedIn = useSelector((state) => state.auth.isLoggedIn);
  const location = useLocation();

  return (
    <div class="fixed bottom-0 w-full bg-green-800 border-t border-green-800 p-4 md:hidden">
      <div class="grid h-full max-w-lg grid-cols-4 mx-auto">
        <Link to='/recipes' className={`icon-link ${location.pathname === '/recipes' ? 'mx-auto active bg-green-600 rounded' : ''}`}>
          <img src={recipesIcon} alt="Recipes" className="mx-auto rounded p-1" />
        </Link>
        <Link to='/inventory' className={`icon-link ${location.pathname === '/inventory' ? 'mx-auto active bg-green-600 rounded' : ''}`}>
          <img src={inventoryIcon} alt="Inventory" className="mx-auto rounded p-1" /> 
        </Link>
        <Link to='/favorites' className={`icon-link ${location.pathname === '/favorites' ? 'mx-auto active bg-green-600 rounded' : ''}`}>
          <img src={favoritesIcon} alt="Favorites" className="mx-auto rounded p-1" />
        </Link>
        <Link to='/shoppinglist' className={`icon-link ${location.pathname === '/shoppinglist' ? 'mx-auto active bg-green-600 rounded' : ''}`}>
          <img src={shoppinglistIcon} alt="Shopping List" className="mx-auto rounded p-1" />
        </Link>
      </div>
    </div>
  );
}
