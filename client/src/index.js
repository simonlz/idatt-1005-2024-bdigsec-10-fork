import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import { store } from "./store/store";
import { Provider } from "react-redux";
import "./index.css";
import Signup from "./pages/Signup";
import Signin from "./pages/Signin";
import Profile from "./pages/Profile";
import RootLayout from "./layouts/RootLayot";
import RecipesPage from "./pages/Recipes";
import ShoppinglistPage from "./pages/Shoppinglist";
import FavoritesPage from "./pages/Favorites";
import InventoryPage from "./pages/Inventory";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<RootLayout />}>
      <Route path="/" element={<Signin />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/signin" element={<Signin />} />
      <Route path="/profile" element={<Profile />} />
      <Route path="/recipes" element={<RecipesPage />} />
      <Route path="/shoppinglist" element={<ShoppinglistPage />} />
      <Route path="/favorites" element={<FavoritesPage />} />
      <Route path="/inventory" element={<InventoryPage />} />
    </Route>
  )
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
