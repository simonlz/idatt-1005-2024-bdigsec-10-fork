const express = require("express");
const cors = require("cors");
const app = express();
const mysql = require("mysql2");
const bcrypt = require("bcrypt");
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const db = mysql.createPool({
  connectionLimit: 10,
  host: "mysql.stud.ntnu.no",
  user: "fredeal_idatt1005",
  password: "fredeal_idatt1005",
  database: "fredeal_idatt1005",
});

const saltRounds = 10;

app.post("/signup", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  // Check if the username already exists
  db.query(
    "SELECT * FROM Users WHERE username = ?",
    [username],
    (err, rows) => {
      if (rows.length > 0) {
        res.status(409).send("Username already exists");
      } else {
        bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
          if (err) {
            res.status(400).send("Could not hash password");
          } else {
            db.query(
              "INSERT INTO Users (username, password) VALUES (?, ?)",
              [username, hashedPassword],
              (err, result) => {
                if (err) {
                  console.log(err);
                  res.status(500).send("Error creating user");
                } else {
                  res.status(201).send({ username: username });
                }
              }
            );
          }
        });
      }
    }
  );
});

app.post("/signin", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  db.query(
    "SELECT * FROM Users WHERE username = ?",
    [username],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        if (result.length > 0) {
          bcrypt.compare(password, result[0].password, (err, response) => {
            if (response) {
              res.send({ username: username });
            } else {
              res.status(400).send("Wrong username/password combination");
            }
          });
        } else {
          res.status(400).send("User does not exist");
        }
      }
    }
  );
});

//Get all recipes

app.get("/recipes", (req, res) => {
  db.query("SELECT * FROM Recipes", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

//get type of favorite recipes
app.get("/favorites/:username/:type", (req, res) => {
    const type = req.params.type;
    const username = req.params.username;
    db.query("SELECT * FROM FavoriteRecipe JOIN Recipes ON FavoriteRecipe.recipeId = Recipes.recipeId WHERE type = ? AND username = ?", [type, username], (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
  });

  //remove favorite recipe
app.delete("/favorites/:favoriteId", (req, res) => {
    const favoriteId = req.params.favoriteId;
    db.query("DELETE FROM FavoriteRecipe WHERE favoriteId = ?", [favoriteId], (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send("Deleted from shoppinglist");
        }
    });
});


// get all favorite recipes
app.get("/favorites/:username", (req, res) => {
  const username = req.params.username;
  db.query(
    "SELECT * FROM FavoriteRecipe JOIN Recipes ON FavoriteRecipe.recipeId = Recipes.recipeId WHERE username = ?", 
    [username],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//post recipe to favorite recipe
app.post("/favorites", (req, res) => {
    const username = req.body.username;
    const recipeId = req.body.recipeId;
    db.query(
      "SELECT * FROM FavoriteRecipe WHERE username = ? AND recipeId = ?",
      [username, recipeId],
      (err, rows) => {
        if (err) {
          console.log(err);
          res.status(500).send("Internal Server Error");
          return;
        }

        if (rows.length > 0) {
          // The combination of username and recipeId already exists, so it's already favorited
          res.status(400).send("Recipe already favorited by this user");
          return;
        }

        // If the combination doesn't exist, then insert it into the FavoriteRecipe table
        db.query(
          "INSERT INTO FavoriteRecipe (username, recipeId) VALUES (?, ?)",
          [username, recipeId],
          (err, result) => {
            if (err) {
              console.log(err);
              res.status(500).send("Internal Server Error");
              return;
            }
            res.send("Added to favorites!");
          }
        );
      }
    );
});


//Get RecipeIngredients

app.get("/recipes/ingredients", (req, res) => {
  const sql = `SELECT *
    FROM Recipes
    JOIN RecipeIngredients ON Recipes.recipeId = RecipeIngredients.recipeId
    JOIN Ingredients ON RecipeIngredients.ingredientId = Ingredients.ingredientId;`;
  db.query(sql, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});
//Get recipes by type

app.get("/recipes/:type", (req, res) => {
  const type = req.params.type;
  db.query("SELECT * FROM Recipes WHERE type = ?", [type], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

//Get all ingredients

app.get("/shoppinglist", (req, res) => {
  db.query("SELECT * FROM Ingredients", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

// Get all shoppinglistitems for a user
app.get("/shoppinglist/:username", (req, res) => {
  const username = req.params.username;
  db.query(
    "SELECT * FROM ShoppinglistItems JOIN Ingredients ON ShoppinglistItems.ingredientId = Ingredients.ingredientId WHERE username = ?",
    [username],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//ShoppinglistItems: listitemId, username, ingredientId, quantity, unit
//add shoppinglistitem

app.post("/shoppinglist", (req, res) => {
  const username = req.body.username;
  const ingredientId = req.body.ingredientId;
  const quantity = req.body.quantity;
  const unit = req.body.unit;
  db.query(
    "INSERT INTO ShoppinglistItems (username, ingredientId, quantity, unit) VALUES (?, ?, ?, ?)",
    [username, ingredientId, quantity, unit],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Added to shoppinglist");
      }
    }
  );
});

//delete from shoppinglist
app.delete("/shoppinglist/:listItemId", (req, res) => {
  const listItemId = req.params.listItemId;
  db.query(
    "DELETE FROM ShoppinglistItems WHERE listItemId = ?",
    [listItemId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Deleted from shoppinglist");
      }
    }
  );
});

//Get shoppinglistitems

app.get("/shoppinglist/:username", (req, res) => {
  const username = req.params.username;
  db.query(
    "SELECT * FROM ShoppinglistItems WHERE username = ?",
    [username],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// add new inventoryitem
app.post("/inventory", (req, res) => {
  const username = req.body.username;
  const ingredientId = req.body.ingredientId;
  const quantity = req.body.quantity;
  const unit = req.body.unit;
  db.query(
    "INSERT INTO InventoryItems (username, ingredientId, quantity, unit) VALUES (?, ?, ?, ?)",
    [username, ingredientId, quantity, unit],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Added to shoppinglist");
      }
    }
  );
});

app.get("/inventory/:username", (req, res) => {
  const username = req.params.username;
  db.query(
    "SELECT * FROM InventoryItems JOIN Ingredients ON InventoryItems.ingredientId = Ingredients.ingredientId WHERE username = ?",
    [username],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// delete from inventoryitem
app.delete("/inventory/:inventoryItemId", (req, res) => {
  const inventoryItemId = req.params.inventoryItemId;
  db.query(
    "DELETE FROM InventoryItems WHERE inventoryItemId = ?",
    [inventoryItemId],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send("Deleted from inventory");
      }
    }
  );
});

app.listen(8080, () => {
  console.log("Server is running on port 8080");
});
